let display = document.querySelector(".input");
let buttons = document.querySelectorAll(".calc-btns button");

buttons.forEach(button => {
    button.addEventListener("click", () => {
        btnValue = button.value;

        if (btnValue === "C") {
            display.value = ""
        } else if (btnValue === "sqrt") {
            display.value = Math.sqrt(display.value)
        } else if (btnValue === "%") {
            display.value = display.value / 100
        } else if (btnValue === "=") {
            display.value = eval(display.value)
        } else{
            display.value += btnValue
        }


    })
})
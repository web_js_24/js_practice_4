let numA = document.querySelector(".num-a");
let numB = document.querySelector(".num-b");
let result = document.querySelector(".display");
let submit = document.querySelector(".submit");

submit.addEventListener("click", () => {
    result.innerHTML = "";

    let a = parseInt(numA.value);
    let b = parseInt(numB.value);

    for (let i = 1; i <= 30; i++) {
        let randomNumber = Math.floor(Math.random() * (b - a + 1)) + a;
        result.innerHTML += `${i}) ${randomNumber} <br>`;
    }
});
